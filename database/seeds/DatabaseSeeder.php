<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
         DB::table('users')->insert([
        [
            'name' => 'Administrador',
            'email' => 'admin@admin.com.br',
            'password' => bcrypt('123456'),
            'created_at' => '2018-08-18',
         ],
         [
            'name' => 'Gerente',
            'email' => 'gerente@gerente.com.br',
            'password' => bcrypt('123456'),
            'created_at' => '2018-08-18',
         ],
         [
            'name' => 'Supervisor',
            'email' => 'supervisor@supervisor.com.br',
            'password' => bcrypt('123456'),
            'created_at' => '2018-08-18',
         ],
         [
            'name' => 'Operador',
            'email' => 'operador@Operador.com.br',
            'password' => bcrypt('123456'),
            'created_at' => '2018-08-18',
         ]
        ]);

        
        DB::table('permissions')->insert([
            [
            'name'  =>  'Administrar sistema',
            'guard_name' => 'web'            
        ],
        [
            'name'  =>  'Gerenciar sistema',
            'guard_name' => 'web'            
        ],
        [
            'name'  =>  'Supervisionar sistema',
            'guard_name' => 'web'            
        ],
        [
            'name'  =>  'Operar sistema',
            'guard_name' => 'web'            
        ],
        [
            'name'  =>  'Acessar usuários',
            'guard_name' => 'web'            
        ],
        [
            'name'  =>  'Incluir usuários',
            'guard_name' => 'web'            
        ],
        [
            'name'  =>  'Alterar usuários',
            'guard_name' => 'web'            
        ],
        [
            'name'  =>  'Excluir usuários',
            'guard_name' => 'web'            
        ],
        [
            'name'  =>  'Acessar funções',
            'guard_name' => 'web'            
        ],
        [
            'name'  =>  'Incluir funções',
            'guard_name' => 'web'            
        ],
        [
            'name'  =>  'Alterar funções',
            'guard_name' => 'web'            
        ],
        [
            'name'  =>  'Excluir funções',
            'guard_name' => 'web'            
        ],
        [
            'name'  =>  'Acessar permissões',
            'guard_name' => 'web'            
        ],
        [
            'name'  =>  'Incluir permissões',
            'guard_name' => 'web'            
        ],
        [
            'name'  =>  'Alterar permissões',
            'guard_name' => 'web'            
        ],
        [
            'name'  =>  'Excluir permissões',
            'guard_name' => 'web'            
        ],
        [
            'name' => 'Administrar usuários',
            'guard_name' => 'web'
        ]
        ]);

        DB::table('roles')->insert([
            [
            'name'  =>  'Admin',
            'guard_name' => 'web'            
        ],
        [
            'name'  =>  'Gerente',
            'guard_name' => 'web'            
        ],
        [
            'name'  =>  'Supervisor',
            'guard_name' => 'web'            
        ],
        [
            'name'  =>  'Operador',
            'guard_name' => 'web'            
        ]
        ]);

        DB::table('model_has_roles')->insert([
            [
               'role_id'  =>  1,
               'model_type' => 'App\User', 
               'model_id' => 1
            ],  
            [
               'role_id'  =>  2,
               'model_type' => 'App\User', 
               'model_id' => 2
            ],
            [
               'role_id'  =>  3,
               'model_type' => 'App\User', 
               'model_id' => 3
            ],
            [
               'role_id'  =>  4,
               'model_type' => 'App\User', 
               'model_id' => 4
            ],         
        ]);

        DB::table('role_has_permissions')->insert([
            [
               'role_id'  =>  1,
               'permission_id' => 1
            ],
            [
               'role_id'  =>  1,
               'permission_id' => 5
            ], 
            [
               'role_id'  =>  1,
               'permission_id' => 6
            ], 
            [
               'role_id'  =>  1,
               'permission_id' => 7
            ], 
            [
               'role_id'  =>  1,
               'permission_id' => 8
            ], 
            [
               'role_id'  =>  1,
               'permission_id' => 9
            ], 
            [
               'role_id'  =>  1,
               'permission_id' => 10
            ], 
            [
               'role_id'  =>  1,
               'permission_id' => 11
            ], 
            [
               'role_id'  =>  1,
               'permission_id' => 12
            ], 
            [
               'role_id'  =>  1,
               'permission_id' => 13
            ], 
            [
               'role_id'  =>  1,
               'permission_id' => 14
            ], 
            [
               'role_id'  =>  1,
               'permission_id' => 15
            ], 
            [
               'role_id'  =>  1,
               'permission_id' => 16
            ], 
            [
               'role_id'  =>  1,
               'permission_id' => 17
            ], 
            [
               'role_id'  =>  2,
               'permission_id' => 2
            ],
            [
               'role_id'  =>  2,
               'permission_id' => 5
            ],
            [
               'role_id'  =>  2,
               'permission_id' => 6
            ],  
            [
               'role_id'  =>  2,
               'permission_id' => 9
            ],
            [
               'role_id'  =>  2,
               'permission_id' => 10
            ], 
            [
               'role_id'  =>  2,
               'permission_id' => 13
            ],
            [
               'role_id'  =>  2,
               'permission_id' => 14
            ],
            [
               'role_id'  =>  2,
               'permission_id' => 17
            ],
            [
               'role_id'  =>  3,
               'permission_id' => 3
            ],
            [
               'role_id'  =>  3,
               'permission_id' => 5
            ],
            [
               'role_id'  =>  3,
               'permission_id' => 9
            ],
            [
               'role_id'  =>  3,
               'permission_id' => 13
            ], 
            [
               'role_id'  =>  3,
               'permission_id' => 17
            ],
            [
               'role_id'  =>  4,
               'permission_id' => 4
            ]            
        ]);


       
    }
}
